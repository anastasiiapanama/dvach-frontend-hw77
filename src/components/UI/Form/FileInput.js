import React, {useRef, useState} from 'react';
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles({
    input: {
        display: 'none'
    }
});

const FileInput = ({onChange, name, label}) => {
    const classes = useStyles();
    const inputRef = useRef();
    const [fileName, setFileName] = useState('');

    const activateInput = () => {
        inputRef.current.click();
    };

    const onFileChange = e => {
        if (e.target.files[0]) {
            setFileName(e.target.files[0].name);
        } else {
            setFileName('');
        }

        onChange(e);
    };

    return (
        <>
            <input
                type="file"
                name={name}
                className={classes.input}
                ref={inputRef}
                onChange={onFileChange}
            />
            <Grid container spacing={2} alignItems="center">
                <Grid item xs>
                    <TextField
                        variant="outlined"
                        disabled
                        fullWidth
                        label={label}
                        value={fileName}
                        onClick={activateInput}
                    />
                </Grid>
                <Grid item>
                    <Button variant="contained"
                            onClick={activateInput}
                    >Choose image</Button>
                </Grid>
            </Grid>
        </>
    );
};

export default FileInput;