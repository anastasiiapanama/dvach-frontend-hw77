import axiosApi from "../axios-api";

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';

export const FETCH_COMMENTS_BY_DATE = 'FETCH_COMMENTS_BY_DATE';

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = payload => ({type: FETCH_COMMENTS_SUCCESS, payload});
export const fetchCommentsFailure = () => ({type: FETCH_COMMENTS_FAILURE});

export const createProductSuccess = () => ({type: CREATE_PRODUCT_SUCCESS});

export const fetchByDate = payload => ({type: FETCH_COMMENTS_BY_DATE, payload});

export const fetchComments = () => {
    return async dispatch => {
        try {
            dispatch(fetchCommentsRequest());

            const response = await axiosApi.get('/comments');
            const lastDate = response.data[response.data.length - 1].datetime;
            dispatch(fetchCommentsSuccess({comments: response.data, lastDate}));
        } catch (e) {
            dispatch(fetchCommentsFailure());
        }
    };
};

export const createComments = commentsData => {
    return async dispatch => {
        await axiosApi.post('/comments', commentsData);
        dispatch(createProductSuccess());
        dispatch(fetchComments());
    };
};

export const getLastDateComments = date => {
    return async dispatch => {
        try {
            const response = await axiosApi.get(`/comments?datetime=${date}`);

            let lastDate;

            if (response.data.length > 1) {
                lastDate = response.data[response.data.length - 1].datetime;
            } else {
                lastDate = response.data[0].datetime;
            }

            dispatch(fetchByDate({comments: response.data, lastDate}));
        } catch (e) {
            console.log(e);
        }
    };
};