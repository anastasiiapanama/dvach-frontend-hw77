import {
    FETCH_COMMENTS_BY_DATE,
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS
} from "./actions";

const initialState = {
    comments: [],
    message: '',
    author: '',
    datetime: '',
    commentsLoading: false
};

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state, commentsLoading: true};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.payload.comments, datetime: action.payload.lastDate, commentsLoading: false};
        case FETCH_COMMENTS_FAILURE:
            return {...state, commentsLoading: false};
        case FETCH_COMMENTS_BY_DATE:
            return {...state, comments: state.comment.concat(action.payload.messages), datetime: action.payload.lastDate};
        default:
            return state;
    }
};

export default commentsReducer;