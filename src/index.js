import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, createStore, compose} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import reducer from "./store/reducer";
import {BrowserRouter} from "react-router-dom";
import {NotificationContainer} from "react-notifications";

import './index.css';
import App from './App';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancers = composeEnhancers(applyMiddleware(thunk));
const store = createStore(reducer, enhancers);

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <NotificationContainer />
            <App/>
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
