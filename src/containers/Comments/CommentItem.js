import React from 'react';
import {apiURL} from "../../config";

import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
});

const CommentItem = ({author, message, image, date}) => {
    const classes = useStyles();

    if (image) {
        image = (
            <CardMedia
                image={apiURL + '/uploads/' + image}
                title={author}
                className={classes.media}
            />
        )
    };

    if(!author) {
        author = 'Anonymous';
    };

    return (
        <Grid item direction="column-reverse" xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader author={author}/>
                {image}
                <CardContent>
                    <strong style={{marginLeft: '10px'}}>
                        "{message}"
                    </strong>
                    <p style={{marginLeft: '10px'}}>
                        Author: <strong>{author}</strong> | Date: {date}
                    </p>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default CommentItem;