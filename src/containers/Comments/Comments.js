import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createComments, fetchComments, getLastDateComments} from "../../store/actions";

import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import CommentItem from "./CommentItem";
import Form from "../../components/Form/Form";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Comments = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const comments = useSelector(state => state.comments);
    const lastDate = useSelector(state => state.datetime);
    const loading = useSelector(state => state.commentsLoading);
    const [showReply, setShowReply] = useState(false);
    let interval = null;

    useEffect(() => {
        dispatch(fetchComments());
    }, [dispatch]);

    useEffect(() => {
        if(lastDate) {
            interval = setInterval( () => {
                dispatch(getLastDateComments(lastDate));
            }, 2000);
        };

        return () => clearInterval(interval)
    }, [dispatch, lastDate]);

    const onCommentFormSubmit = async commentData => {
        await  dispatch(createComments(commentData));
    };

    const toggleReply = () => {
        setShowReply(!showReply);
    };

    let replyForm = null;

    if(showReply === true) {
        replyForm = (
            <Form
                onSubmit={onCommentFormSubmit}
            />
        );
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">JS Tread</Typography>
                </Grid>
                <Grid item>
                    <Button color="primary" onClick={toggleReply}>Reply to Tread</Button>
                </Grid>
            </Grid>
            {replyForm}
            <Grid item container direction="column-reverse" spacing={1}>
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress />
                        </Grid>
                    </Grid>
                ) : comments.map(comment => (
                    <CommentItem
                        key={comment.id}
                        author={comment.author}
                        message={comment.message}
                        image={comment.image}
                        date={comment.datetime}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Comments;