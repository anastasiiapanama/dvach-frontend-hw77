import {Switch, Route} from "react-router-dom";

import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Comments from "./containers/Comments/Comments";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";

const App = () => (
    <>
        <CssBaseline />
        <header>
            <AppToolbar />
        </header>
        <main>
            <Container maxWidth="xl">
                <Switch>
                    <Route path="/" exact component={Comments} />
                </Switch>
            </Container>
        </main>
    </>
);

export default App;
